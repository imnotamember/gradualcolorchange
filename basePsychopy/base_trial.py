from psychopy import core, event

import base_data


def task(win,
         inst_screen,
         resp_screen,
         exp_info,
         task_name,
         data_file_name,
         fixation,
         trial_order,
         stimuli_list,
         block_number,
         typical_colors_up,
         typical_colors_down,
         critical_colors_up,
         critical_colors_down,
         frames_per_trial,
         frame_stim,
         widescreen_fix=False   # This is if you are using a widescreen (16:9) monitor,
         ):                     # but want to present in standard (4:3) format.
    trial_clock = core.Clock()
    t_clock = core.Clock()
    response_clock = core.Clock()
    isi = core.StaticPeriod(60, win)
    trials = len(trial_order)
    possible_responses = 0
    number_of_correct_responses = 0
    if widescreen_fix:
        widescreen_fix.draw()
    inst_screen.draw()
    win.flip()
    event.waitKeys(keyList=['space'])
    cumulative_trials = (block_number - 1) * trials
    change_side = 'none'
    response_dictionary = {'left': ['d', 'D', 'f', 'F', 'g', 'G'],
                           'right': ['h', 'H', 'j', 'J', 'k', 'K']
                           }
    for trial in xrange(trials):
        # Prepare variables for each trial from the stimuli list
        current_trial = trial_order[trial]
        circle_color_list = current_trial
        if 0 in current_trial:
            critical_colors = critical_colors_up
            trial_type = 'rw'
        elif 255 in current_trial:
            critical_colors = critical_colors_down
            trial_type = 'wr'
        if 0 in current_trial[:4] or 255 in current_trial[:4]:
            change_side = 'left'
        elif 0 in current_trial[4:] or 255 in current_trial[4:]:
            change_side = 'right'
        else:
            print 'OBJECT NOT ON EITHER SIDE!?!?!?!'

        # Show Fixation for .5 seconds
        if widescreen_fix:
            widescreen_fix.draw()
        fixation.draw()
        win.callOnFlip(t_clock.reset)
        win.callOnFlip(isi.start, .5)
        win.flip()
        fixation.draw()
        isi.complete()

        stim_presentation(win, fixation, frames_per_trial, stimuli_list, circle_color_list, frame_stim,
                          typical_colors_up, typical_colors_down, critical_colors, trial_clock, widescreen_fix)
        resp_screen.draw()
        win.callOnFlip(event.clearEvents)
        win.callOnFlip(response_clock.reset)
        win.flip()
        while response_clock.getTime() < 2:
            resp,\
            resp_time,\
            cor,\
            possible_responses,\
            number_of_correct_responses = key_logger(response_clock,
                                                     response_dictionary[change_side],
                                                     trial,
                                                     number_of_correct_responses
                                                     )
            if resp != 'None':
                break
        if possible_responses:
            accuracy = float(number_of_correct_responses) / possible_responses
        else:
            accuracy = 0.0
        # print resp, resp_time, cor, possible_responses, number_of_correct_responses, accuracy

        base_data.write_data(data_file_name,
                             (exp_info['Participant'],
                          exp_info['Condition'],
                          trial+1,
                          block_number,
                          task_name,
                          response_dictionary[change_side][2],
                          resp,
                          cor,
                          accuracy,
                          resp_time,
                          t_clock.getTime(),
                          change_side,
                          trial_type
                          )
                             )


def stim_presentation(win, fix, frames_to_present, circles, color_list, frame_stim, typical_colors_up,
                      typical_colors_down, critical_colors, trial_clock, widescreen_fix=False):
    for frameN in xrange(int(frames_to_present)):
        if widescreen_fix:
            widescreen_fix.draw()
        for i in xrange(len(circles)):
            if color_list[i] == 8:
                circles[i].fillColor = (255, int(typical_colors_up[frameN]), int(typical_colors_up[frameN]))
            elif color_list[i] == 247:
                circles[i].fillColor = (255, int(typical_colors_down[frameN]), int(typical_colors_down[frameN]))
            else:
                circles[i].fillColor = (255, int(critical_colors[frameN]), int(critical_colors[frameN]))
                # fix.pos = circles[i].pos
            circles[i].draw()
        fix.draw()
        if (frames_to_present / 2) - 20 < frameN < (frames_to_present / 2) + 20:
            frame_stim.draw()
        if frameN == 0:
            win.callOnFlip(trial_clock.reset)
        win.flip()


def key_logger(trial_clock, correct_buttons, possible_trials, correct_tally):
    trial_rt = event.getKeys(timeStamped=trial_clock)
    correct = 0
    response = 'None'
    response_time = 'None'
    if trial_rt:
        response = trial_rt[0][0]
        response_time = trial_rt[0][1]
        if correct_buttons is 'None':
            correct = 'NA'
        else:
            if response in ['escape', 'esc']:
                quit()
            elif response in correct_buttons:
                correct = 1
            correct_tally += correct
            possible_trials += 1
    return response, response_time, correct, possible_trials, correct_tally


def instructions_screen(win, inst_screen):
    inst_screen.draw()
    win.flip()
    event.waitKeys(keyList=['space'])
