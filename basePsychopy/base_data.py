import os
from psychopy import logging


def create_data_file(
        experiment_info,
        task_name,
        header='Participant number, Cumulative trial number, Trial number, Block number, Task, ImageL, ImageR, StimL, '
               'StimR, Correct Response, Response, Correct, Accuracy, Response Time by trial, Response Time by stim, '
               'Trial time, Trial Type, SOA',
        log_file=True):
    """
    
    :param log_file: Create a log file? Log files are more for backup purposes and are a last resort for saving data.
    :param experiment_info: This is your experiment_info variable which you should have created first with
            the _experiment_info.get_info() function.
    :param task_name: Name of your task. (string)
    :param header: These are the headers to each column, separated by commas. (string) 
    :return: file_path: Return a string of the path of the saved data file for using with the write_data() function.
    :return: log_file: If 'log_file' is set to 'True', will return the log file object. Otherwise returns 'False'.
    """
    file_name = experiment_info['Condition'] + '-' + experiment_info['Participant'] + '-' + experiment_info['date'] + \
                                                   '-' + task_name + '.csv'
    folder_name = os.path.join('Data', experiment_info['Participant'], '')
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    file_path = '%s%s' % (folder_name, file_name)
    with open(file_path, 'w') as f:
        f.write(header)
        f.write('\n')
    if log_file:
        log_file = create_log_file(file_path)
    return file_path, log_file


def create_log_file(file_path):
    """
    
    :param experiment_info: This is your experiment_info variable which you should have created first with
            the _experiment_info.get_info() function.
    :param file_path: file_path which is returned from calls to _data.create_data_file()
    :return: log: Logging object to be closed at end of study.
    """
    return logging.LogFile(file_path[:-4] + '.log', level=logging.DEBUG)


def write_data(filename, data):
    with open(filename, 'a') as f:
        row_string = ",".join(map(str, data))
        f.write(row_string + '\n')
