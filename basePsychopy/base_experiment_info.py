# coding=utf-8

from psychopy import data, gui, core


def get_info(experiment_name="experiment", experiment_info={'Participant': '1234'}):
    """
    Get and return info about the experiment session

    :param experiment_name: The name of your experiment.
    :type experiment_name: basestring
    :param experiment_info: The information you want to collect before starting the experiment along with default 
            values.
    :type experiment_info: dict    
    :return experiment_info: A dictionary of the values entered into the GUI at the start of the experiment.
    :type experiment_info: dict
    """
    dlg = gui.DlgFromDict(dictionary=experiment_info, title=experiment_name)
    if not dlg.OK:
        core.quit()  # user pressed cancel
    experiment_info['date'] = data.getDateStr()  # add a simple timestamp
    experiment_info['experiment_name'] = experiment_name
    return experiment_info
