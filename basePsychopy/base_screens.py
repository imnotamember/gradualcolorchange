import glob
import random
from psychopy import visual, monitors


def create_window(fullscreen, size=(1920, 1080), units='deg', widescreen_fix=False):
    mon = monitors.Monitor('default', 51, 44)
    mon.setSizePix(size)
    if widescreen_fix:
        # This is if you are using a widescreen (16:9) monitor, but want to present in standard (4:3) format.
        # If 'widescreen_fix' is set to 'False', it will present to the fullest extent of the
        # 'size' you pass to this function.
        window = visual.Window(size=(1920, 1080), fullscr=fullscreen, units=units, allowGUI=False, monitor=mon,
                               color=[-1, -1, -1])
    else:
        window = visual.Window(size=(1920, 1080), fullscr=fullscreen, units=units, allowGUI=False, monitor=mon)
    frame_rate = window.getActualFrameRate()
    return window, frame_rate


def create_fixation_screen(window):
    return visual.TextStim(win=window, text="+", pos=[0, 0], rgb='black')


def create_instructions_screen(window, instructions, wrap_width=30, units='deg'):
        return visual.TextStim(win=window, text=instructions, wrapWidth=wrap_width, units=units)


def create_stim_screens(window, number_of_target_stim):
    all_stim = sorted(glob.glob('fractal_stimuli/*.png'))
    random.shuffle(all_stim)
    stim_list = []
    for i in xrange(1, number_of_target_stim + 1):
        for j in xrange(number_of_target_stim):
            stim_file = all_stim.pop()
            stim_list.append(visual.ImageStim(win=window, image=stim_file, units='deg'))
    for i in xrange(len(all_stim)):
        stim_file = all_stim.pop()
        stim_list.append(visual.ImageStim(win=window, image=stim_file, units='deg'))
    return stim_list


def create_widescreen_fix(window, new_size=(1024, 768), color=[0, 0, 0]):
    return visual.Rect(window, new_size[0], new_size[1], fillColor=color, autoLog=False)
