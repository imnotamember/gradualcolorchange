import random
from math import sqrt
from matplotlib import pyplot


def create_arc(center, number_of_points, graph=False):
    # circle equation: radius**2 = (x - center[0])**2 + (y - center[1])**2
    # circle equation: radius = sqrt((x - center[0])**2 + (y - center[1])**2)
    radius = sqrt((0 - center[0])**2 + (0 - center[1])**2)
    arc = []
    for i in xrange(number_of_points):
        # circle equation: (x - center[0])**2 + (y - center[1])**2 = radius**2
        # circle equation: (y - center[1])**2 = radius**2 - (x - center[0])**2
        # circle equation: y - center[1] = sqrt(radius**2 - (x - center[0])**2)
        # circle equation: y = sqrt(radius**2 - (x - center[0])**2) + center[1]
        arc.append(sqrt(radius**2 - (i - center[0])**2) + center[1])
    if graph:
        pyplot.plot(arc)
        pyplot.show()
    return arc


def correct_arc(arc, line, additive_subtractive='additive', graph=False):
    arc_length = len(arc)
    line_length = len(line)
    truncate_interval = (arc_length - line_length) / 2
    truncate_pre = truncate_interval
    truncate_post = arc_length - truncate_interval
    arc = arc[truncate_pre:truncate_post]
    transform = []
    for i in xrange(len(arc)):
        if additive_subtractive == 'additive':
            transform.append(line[i] + arc[i])
        elif additive_subtractive == 'subtractive':
            transform.append(line[i] - arc[i])
        else:
            "Unknown command, must use 'additive' or 'subtractive' as values."
    if graph:
        fig, ax = pyplot.subplots()
        ax.plot(transform, 'g-', label='Transformed curve')
        ax.plot(arc, 'b-', label='Arc')
        ax.plot(line, 'r-', label='Line')
        legend = ax.legend(loc='upper center', shadow=True, fontsize='x-large')
        pyplot.show()
    return transform


def create_trial_order(number_of_trials, number_of_stim, possible_stim_list):
    blocks = number_of_trials / number_of_stim
    block_list = []
    for i in xrange(blocks):
        for j in number_of_stim:
            block_list.append(random.shuffle(possible_stim_list[j]))


def test(l):
    b = None
    for i in xrange(1000):
        random.shuffle(l)
        if l == b:
            print l
            print b
            print i
            print 'noWay!'
            break
        b = list(l)


def create_trials(blocks, critical_value):
    colors_a = (8, 247, 8, 247, 8, 247, 8)
    colors_b = (8, 247, 8, 247, 8, 247, 247)
    block = []
    for b in xrange(2, blocks + 2):
        for i in xrange(2, 10):
            if (b % 2 == 0 and i % 2 == 0) or (b % 2 != 0 and i % 2 != 0):
                temp = list(colors_a)
            else:
                temp = list(colors_b)
            random.shuffle(temp)
            temp.insert(i - 2, critical_value)
            block.append(temp)
    random.shuffle(block)
    return block
