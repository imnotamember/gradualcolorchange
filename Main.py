# coding=utf-8

import random
from psychopy import visual, core

from basePsychopy import base_screens, base_trial_organizer, base_experiment_info, base_trial, base_data

# Troubleshooting?
troubleshooting = False

expInfo = base_experiment_info.get_info("TOJ Study", experiment_info={'Participant': '', 'Condition': ''})
if expInfo['Participant'] == '':
    expInfo['Participant'] = 'unknown--' + expInfo['date']
    print expInfo['Participant']

try:
    win, frame_rate = base_screens.create_window(fullscreen=True, units='pixels', widescreen_fix=True)
    if frame_rate:
        print frame_rate
    else:
        frame_rate = 60

    # Trial duration time in ms
    trial_duration = 4000
    # Calculate refresh rate in ms
    refresh_rate = 1000 / frame_rate
    # Calculate the number of screen refreshes in the designated trial time (trial_duration)
    # # i.e.: typical refresh rate = 16.667ms (60 frames per second), so 4000/16.667 = 240 frames per trial
    frames_per_trial = int(trial_duration / refresh_rate)
    print refresh_rate, frames_per_trial
    if frames_per_trial != 240:
        frames_per_trial = 240


    half_x_size = win.size[0] / 2
    half_y_size = win.size[1] / 2

    # Defaults
    targets = 3
    groups = 2
    total_stim = targets * groups
    blocks = 3
    sl_blocks = 6

    # Circle locations
    circle_locations = ((-224, -192),
                        (-128, -96),
                        (-352, -224),
                        (-384, 64),
                        (128, -32),
                        (352, 0),
                        (320, 256),
                        (256, -192)
                        )

    updated_circle_locations = []
    for i in circle_locations:
        updated_circle_locations.append((i[0] + half_x_size, i[1] + half_y_size))
    # print updated_circle_locations

    stim_list = []
    for x, y in circle_locations:
        stim_list.append(visual.Circle(win,
                                       48,
                                       pos=(x, y),
                                       lineWidth=0,
                                       fillColor=(255, 0, 0),
                                       units='pixels',
                                       fillColorSpace='rgb255',
                                       autoLog=False))
        #stim_list[-1].draw()

    welcome_instructions = """                                                     Welcome to the study

    In this experiment, you will see various red circles gradually change colors.

    At one moment all of them will have the same shade of red except one.

    The different one could be lighter or darker.

    Also at the same moment, a framed square will appear.

    This is a cue to tell you that at this moment, everything is the same color except for one.

    Your job is to find the one that is different from everything else.

    But you may not make a response until the computer tells you to do so.

    If you think the different one is on the left side of the screen, press 'f' key.

    If you think the different one is on the right side of the screen, press 'j' key.



    You may begin at any time by pressing the spacebar."""

    response_instructions = """Press 'f' for left

    or 'j' for right."""

    # Create all possible color changes
    # # Typical colors (linear addition/subtraction resulting in red->white or 'up' and white->red or 'down')
    typical_colors_up = range(8, 248)
    typical_colors_down = range(248, 8, -1)
    # # Additive arc for critical trials, change the decimal value in the 2nd tuple index to alter sensitivity to change
    # # (larger decimal value = less distinguishable; smaller decimal value = more distinguishable)
    arc = base_trial_organizer.create_arc(((256 * 0.5), -(256 * 1.5)), 256)
    # # Using the additive arc, create a new list of critical colors to match the typical red->white and white->red lists
    critical_colors_up = base_trial_organizer.correct_arc(arc, typical_colors_up)
    critical_colors_down = base_trial_organizer.correct_arc(arc, typical_colors_down, additive_subtractive='subtractive')

    trial_order_0 = base_trial_organizer.create_trials(4, 0)
    trial_order_0.__delslice__(24, 32)
    trial_order_255 = base_trial_organizer.create_trials(4, 255)
    trial_order_255.__delslice__(24, 32)
    trial_order = list(trial_order_0)
    trial_order.extend(list(trial_order_255))
    random.shuffle(trial_order)

    widescreen_fix_screen = base_screens.create_widescreen_fix(win)
    fixation_screen = base_screens.create_fixation_screen(win)
    instruction_screen = base_screens.create_instructions_screen(win, welcome_instructions, wrap_width=1000, units='pixels')
    response_screen = base_screens.create_instructions_screen(win, response_instructions, wrap_width=1000, units='pixels')
    # Create data files
    data_header = "Participant,Condition,Trial Number,Block Number,Task Name,Correct Response,Response,Correct," \
                  "Accuracy,Response Time,Total Time,Target Side,Trial Type(rw=red->white;wr=white->red)"
    data_file_name, log_file = base_data.create_data_file(expInfo, 'GradualColorChange', header=data_header)

    base_trial.task(win, instruction_screen, response_screen, expInfo, "GradualColorChange", data_file_name, fixation_screen,
                    trial_order, stim_list, 1, typical_colors_up, typical_colors_down,
                    critical_colors_up, critical_colors_down, frames_per_trial,
                    visual.Rect(win, width=1024-5, height=768-5, lineColor="RED", lineWidth=10, autoLog=False),
                    widescreen_fix=widescreen_fix_screen
                    )

    logging.flush()
    core.quit()
except:
    logging.flush()
    core.quit()
